
var recordBT = null;
var recordView = null;
var recordImageView =null;
var closeImageView=null;
var buttonView= null;
var recordButton=null;
var playButton=null;
var saveButton=null;
var slider = null;
var selectionView=null;
var selectionView_v=null;
//var vibrationView=null;

var responseIndex=0;
var digitIndex=0;
var unitIndex=0;
var desiredAnswerId=0;
var soundName= "Short Notice";
var vibrationName="Vibration1";

var timePeriodInMs = 5000;
var recordedPattern = [];
var finalPattern = [];
var enableVibrations = false;

var Vibration1 = [0,10,10,10,1000,1000,500,1000,50];
var Vibration2 = [0,700,500,700,50,700,600,1000,500,50,1000,50,50,50];
var Vibration3 = [0,50,50,50,50,50,50,50,505,200,50,50,50,50,50,50,505,50];
var CustomVibration = [];
var checkString = 'Vibration';


// Initializing other controls of skillView

	
	
   
   $.skillview.addEventListener('android:back', function(){
   $.skillview.close();
 //  var addSkillController = Alloy.createController("index", skillModel);
  // var skillList = addSkillController.getView();
  // skillList.open(); 
   });



var desiredAnswer = nativecontrols.createRadioGroup({
	width : '60%',
	height : Ti.UI.SIZE,
	left : 0,
	textColor : "",
	selectedIndex : 0,
	layoutType : "vertical",
	buttons : [{
		id : 1,
		text : 'Yes'
	}, {
		id : 2,
		text : 'No'
	}]
});




  
 


function desiredAnswerChange(e)
{
	desiredAnswerId = e.result.selectedIndex;	
}

desiredAnswer.addEventListener('change', desiredAnswerChange); 


    





/*

function vibrateRecord()
{
    Ti.Media.vibrate([0,50]);
	vibrateDuration = vibrateDuration+100;
	vibrate.push(vibrateDuration);
    silentDuration=0;	
}


function silenceRecord()
{
	 vibrateDuration=0;
     silentDuration =0;
     silentDuration = silentDuration+500;
     vibrate.push(silentDuration);
}


function playVibration()
{
		Ti.Media.vibrate(vibrate);
}

*/





/*
function newDisplay(e)
{
	if($.ResponsePicker.getSelectedRow(0).title ==checkString){		
	
	recordBT = Ti.UI.createButton({
		backgroundColor:'#686363',
		width:'90',
		height:'50',
		title:"Record Vibration"
	});
	
		
	responseIndex = e.rowIndex;	
	recordBT.addEventListener('click', recordWindow);	
	$.scrollView.add(recordBT);	
	}

else if(recordBT!=null)
  $.scrollView.remove(recordBT);


 
}
*/


function customVibrationRecorder()
 {
   	recordView = Ti.UI.createView({
   		id:'recordView',
		height:'200',
		width:'200',
		zIndex:50,
		top:'0',
		left:'0',
		backgroundColor:'#494946',
	});
	
	recordImageView = Ti.UI.createImageView({
		image:'/images/record.png',
		backgroundColor:'transparent',
		width:'150',
		height:'150',
		bottom:'50',
		zIndex:50
	});
	
	closeImageView = Ti.UI.createImageView({
	    image:'/images/close2.png',
		backgroundColor:'transparent',
		width:'30',
		height:'30',
		right:'0',
		top:'0',
		zIndex:50
	});function displaySelection()
{ 	
	if(selectionView_s!=null)
	{
		$.responseTypeView.remove(selectionView_s);
		selectionView_s = null;
    
                      
   selectionView_v.add(vibrationIcon);
   selectionView_v.add(responseTypeName_v);
   selectionView_v.addEventListener('click', displayVibrations);		
   $.responseTypeView.add(selectionView_v);			
	}	
	else if(selectionView_v!=null)
	{
		$.responseTypeView.remove(selectionView_v);
		selectionView_v = null;
		
   selectionView.add(playIcon);
   selectionView.add(responseTypeName);
   selectionView.addEventListener('click', displaySounds);
   $.responseTypeView.add(selectionView); 	
	}
	
}

	
/*	buttonView = Ti.UI.createView({
		height:'50',
		width:'200',
		layout:'horizontal',
		zIndex:'3',
		bottom:'0',
		backgroundColor:'white'
}); */
	
	recordButton = Ti.UI.createButton({
		backgroundColor:'#787878',
		width:'33%',
	    height:'50',
	    title:'Record',
	    bottom:'0',
	    color:'white',
	    left:'0',
	    borderColor:'white',
	    borderWidth:'1',
	    zIndex:50
	}); 

	
	playButton = Ti.UI.createButton({
		backgroundColor:'#787878',
	    height:'50',
		width:'33%',
		title:'Play',
		color:'white',
		bottom:'0',
		borderColor:'white',
	    borderWidth:'1',
	    zIndex:50
	});
	
	
	saveButton = Ti.UI.createButton({
		backgroundColor:'#787878',
	    height:'50',
		width:'33%',
		title:'Save',
		color:'white',
		bottom:'0',
		right:'0',
		borderColor:'white',
	    borderWidth:'1',
	    zIndex:50
	});
	
	closeImageView.addEventListener('click', function(){
	//$.scrollView.remove(recordView);
	vibrationView.removeAllChildren();
    vibrationView.add(closeVibrationChoices);
	vibrationView.add(vibrationChoices);
	recordView=null; 
});



function beginRecordingSoon() {
    recordButton.backgroundColor = "yellow";
    setTimeout(startRecording, 1000);
}

function startRecording() {
    var startTime = new Date().getTime();
    recordedPattern = [];
    recordedPattern.push(startTime);
    enableVibrations = true;
    recordButton.backgroundColor = "green";
    setTimeout(endRecording, timePeriodInMs);
}

function endRecording() {
    stopVibrating();
    enableVibrations = false;
    var endTime = new Date().getTime();
    recordedPattern.push(endTime);
    recordButton.backgroundColor = "#787878";

    var previousTime = recordedPattern[0];
    Ti.API.info("recorded pattern:");
    Ti.API.info(recordedPattern);
    finalPattern = [];
    for (var i = 1; i < recordedPattern.length; ++i) {
        var currentTime = recordedPattern[i];
        finalPattern.push(currentTime - previousTime);
        previousTime = currentTime;
    }
  //  Ti.API.info(finalPattern);

   // setTimeout(playbackFinal, 3000);
}

function playbackFinal() {
  //  $.recordButton.backgroundColor = "red";
    Ti.Media.vibrate(finalPattern);
}

function startVibrating() {
    if (enableVibrations) {
        recordedPattern.push(new Date().getTime());
        Ti.Media.vibrate([0, timePeriodInMs]);
    }
}

function stopVibrating() {
    if (enableVibrations) {
        recordedPattern.push(new Date().getTime());
        Ti.Media.vibrate([0, 1]);
    }
}




    recordButton.addEventListener('click', beginRecordingSoon);
    recordImageView.addEventListener('touchstart', startVibrating);
    recordImageView.addEventListener('touchend', stopVibrating);
	playButton.addEventListener('click', playbackFinal);
	
	saveButton.addEventListener('click', function(){
		customVibration=finalPattern;
		Ti.API.info(customVibration);
	    vibrationView.removeAllChildren();
        vibrationView.add(closeVibrationChoices);
	    vibrationView.add(vibrationChoices);
	    recordView=null;
	});
	
/*	buttonView.add(recordButton);
	buttonView.add(playButton);
	*/
	vibrationView.removeAllChildren();
	recordView.add(closeImageView);
	recordView.add(recordImageView);
	recordView.add(recordButton);
    recordView.add(saveButton);
    recordView.add(playButton);
    vibrationView.add(recordView);
	//$.vibrationRecordView.add(recordView);
//	$.scrollView.add(recordView);
   	
   }
                                
                  
                  
   selectionView_s = Ti.UI.createView({
   layout:'horizontal',
   left:48,
  // height:'Ti.UI.SIZE',
  // width:'Ti.UI.SIZE'
    });
                 
  var playIcon = Ti.UI.createImageView({
   width:'35',
   height:'35',
   image:'/images/play.png'
     });
  var responseTypeName = Ti.UI.createLabel({
  	id:'responseTypeName',
    text:"   " +soundName,
    color:'#c4c1b8'
     });
   selectionView_s.add(playIcon);
   selectionView_s.add(responseTypeName);
   selectionView_s.addEventListener('click', displaySounds);
  
  
   $.responseTypeView.add(selectionView_s); 
  
  
  
   


                        
                                  
                                  
                                     
    











$.skillview.addEventListener('click', function(e)
{
	 if(vibrationView!=null)
      {
        $.mainView.remove(vibrationView);	
	//	recordView = null;
	  }
	else if(slider!=null)
	  {
	  	$.mainView.remove(slider);
	//  	slider=null;
	  }
	  else if(slider==null && recordView==null)
        $.skillview.close();	  
    //  alert(e.value);
	
});










if(args.length>1)
{
		
	Ti.API.info(args[2]);	
	Ti.API.info(args[3]);
	Ti.API.info(args[4]);
	Ti.API.info(args[5]);
	
	$.skillview.open();
	$.skillName.value = args[0];
	$.skillQuestion.value = args[1];
	radiogroup.selectedIndex= parseInt(args[2]);
	if(args[3]!=0)$.ResponsePicker.setSelectedRow(0,args[3],false);
	$.digitSliderLabel.text = args[4];
	$.unitPicker.setSelectedRow(0,args[5],false);

}


