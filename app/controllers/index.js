var args = arguments[0] || {};

var skillModel = Alloy.Collections.skill;
skillModel.fetch();


/*
var tbl_data = [
	{title:'Row 1'},
	{title:'Row 2'},
	{title:'Row 3'}
];


var table = Ti.UI.createTableView({
	data:tbl_data
});

*/
$.mainWin.open();

//$.mainWin.add(table);

/*$.mainWin.addEventListener('android:back', function(){
	 $.mainWin.close();
    var editSkillController = Alloy.createController("skillview", args);
    var editSkillView = editSkillController.getView();
	
});
*/
function clickNew()
{  
 var args = {};
   var addSkillController = Alloy.createController("skillview", args);
   var addSkillView = addSkillController.getView();
   addSkillView.open(); 
}


function openSkill(event)
	{
    var selectedSkill = event.source;
    
    var args = [];
    var skillFetch = skillModel.where({name: selectedSkill.title})[0];
    args[0] =  skillFetch.get('name');
    args[1] =  skillFetch.get('question');
    args[2] =  skillFetch.get('desiredAnswer') ;
    args[3] =  skillFetch.get('responseType') ;
    args[4] =  skillFetch.get('frequencyEvery') ;
    args[5] =  skillFetch.get('frequencyUnit') ;
   // $.mainWin.close();
    var editSkillController = Alloy.createController("skillview", args);
    var editSkillView = editSkillController.getView();
    editSkillView.open(); 

	}
	
/*	
	var selectedSituation = event.source;
	
    var situationModel = allSituations.where({name: selectedSituation.name})[0];
    var args = { data: situationModel.get('data') };
*/

function resetSkills()
{
    _.invoke(skillModel.toArray(), 'destroy');
}


