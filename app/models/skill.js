exports.definition = {
    config: {
        columns: {
/*
 "id": "int",
 "name": "text",
 "data": "text"
 */
 "name": "text",
 "question": "text",
 "desiredAnswer": "text",
 "responseType": "text",
 "frequencyEvery": "text",
 "frequencyUnit": "text"

        },
        adapter: {
            type: "sql",
            collection_name: "skill"
        }
	},
	extendModel: function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
		});

		return Model;
	},
	extendCollection: function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here
		});

		return Collection;
	}
};